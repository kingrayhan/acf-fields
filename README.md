![acf.jpg](https://bitbucket.org/repo/XXr89bL/images/4123516058-acf.jpg)
# Advance Custom Fields Premium addons



# Plugin URL: https://wordpress.org/plugins/advanced-custom-fields/



# Addon list
---
* [Repeater Field](https://www.advancedcustomfields.com/add-ons/repeater-field/)
* [Gallery Field](https://www.advancedcustomfields.com/add-ons/gallery-field/)
* [Flexible Content Field](https://www.advancedcustomfields.com/add-ons/flexible-content-field/)

## Click download button bellow to download these addons.

[![download.gif](https://bitbucket.org/repo/XXr89bL/images/2179189739-download.gif)](https://bitbucket.org/kingrayhan/acf-fields/get/master.zip)

> # Don't use these addons in your commercial project. You can only download and use these addons just only for study purpose.
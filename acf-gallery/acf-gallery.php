<?php
/*
Plugin Name: Advanced Custom Fields: Gallery Field
Plugin URI: http://www.advancedcustomfields.com/
Description: This premium Add-on adds a gallery field type for the Advanced Custom Fields plugin
Version: 1.1.0
Author: Elliot Condon
Author URI: http://www.elliotcondon.com/
License: GPL
Copyright: Elliot Condon
*/


add_action('acf/register_fields', 'acfgp_register_fields');

function acfgp_register_fields()
{
	get_template_part('gallery.php');
}


/*
*  Update
*
*  if update file exists, allow this add-on to connect and recieve updates.
*  all ACF premium Add-ons which are distributed within a plugin or theme, must have the update file removed.
*
*  @type	file
*  @date	13/07/13
*
*  @param	N/A
*  @return	N/A
*/

if( file_exists(  get_template_directory() . '/kt-core/acf-fields/add-ons/acf-gallery/acf-gallery-update.php' ) )
{
	get_template_part('/acf-gallery-update.php' );
}

?>
